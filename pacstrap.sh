#!/bin/bash 

echo "Install reflector and update mirrors"

pacman -S reflector

reflector --verbose --latest 5 --sort rate --country "United States" --save /etc/pacman.d/mirrorlist

echo "Performing pacstrap install"

pacstrap -i /mnt base base-devel linux openssh git vim

echo "Generate /etc/fstab"

genfstab -pU /mnt >> /mnt/etc/fstab

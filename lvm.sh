#!/bin/bash

ROOTFS=16G

echo "Performing LVM volume creations"
sleep 3

pvcreate /dev/mapper/luks
vgcreate vg0 /dev/mapper/luks
lvcreate --size $ROOTFS vg0 --name root
lvcreate -l +100%FREE vg0 --name home

echo "Format LVM partitions"

mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/mapper/vg0-home
mkfs.ext2 /dev/sda1

echo "Mount the new system"

mount /dev/mapper/vg0-root /mnt
mkdir /mnt/{boot,home}
mount /dev/sda1 /mnt/boot
mount /dev/mapper/vg0-home /mnt/home

echo "LVM creation and mounting complete"
